#!/bin/sh

python update_age_BSCinput.py -i BSCinput_TDMRA.par -l 0 -u 60000 -n 10 --dist Bison_dates_distribution.csv --prop 0.117647058823529
bash batch_run_BSC.sh
python replace_tip_names.py
rm *_mut_trees.trees *_true_trees.trees *_stat.csv *.gen mut_hits_distr.sum gamma_rates.distr
Rscript run_Duchene_script.R 1e-8 0.05 627 3 10
