#!/usr/bin/env python

'''
script to update age samples BSCinput
Usage: python update_age_BSCinput.py -i BSCinput_TDMRA.par -l 0 -u 60000 -n 100 --dist Bison_dates_distribution.csv --prop 0.117647058823529
Diego F. Alvarado-Serrano, Jun 18, 2015
'''

import os, re, numpy as np, random
from scipy import stats
from optparse import OptionParser

usage = "usage: %prog [options] arg"
parser = OptionParser(usage=usage)
parser.add_option("-i",dest="Input", help="path to the template par file")
parser.add_option("-l",dest="Lower", help="minimum value for uniform distribution for sampling age")
parser.add_option("-u",dest="Upper", help="maximum value for uniform distribution for sampling age")
parser.add_option("-n",dest="fileNumber", help="number of independent .par files to create")
parser.add_option("--dist",dest="Distribution", default='uniform', help="number of independent .par files to create")
parser.add_option("--prop",dest="PropExtant", default='', help="proportion of extant date tips")
(options,args) = parser.parse_args() 

infilename = options.Input
lowbound = int(options.Lower)
uppbound = int(options.Upper)
nfiles = int(options.fileNumber)
distr = options.Distribution
propext = float(options.PropExtant)

with open('parfilelist.bat', 'w') as batchfile:
    for i in range(nfiles):
        replacement = '-%d.par' %i
        outfilename = infilename.replace('.par', replacement)
        print >> batchfile, outfilename
        
        with open(outfilename, 'w') as outfile:
            with open(infilename, 'r') as infile:
                pattern = re.compile('{U:[0-9]+,[0-9]+}')
                m = 0
                for line in infile:
                    if line.strip().endswith('sample groups'):
                        ndemes = int(line.strip().split()[0])
                        extantsamples = random.sample(range(ndemes), (int(ndemes*propext)+1))
                    find = re.search(pattern, line)
                    if find:
                        m += 1
                        if (distr == 'uniform'):
                            age = int(np.random.uniform(lowbound,uppbound))
                        else:
                            distrfile = open('Bison_dates_distribution.csv', 'r')
                            geno = np.empty([1,2])
                            n = 0
                            for line2 in distrfile:
                                n += 1
                                if n > 1:
                                    row = [float(x) for x in line2.strip().split(',')]
                                    geno = np.vstack((geno, row))
                            geno = np.delete(geno,0,0)
                            distrfile.close()
                            customdist = stats.rv_discrete(b=max(geno[:,0].astype(int)), values=(geno[:,0].astype(int), geno[:,1]))
                            age = int(customdist.rvs(size=1)[0])
                            #assuring the proportion of extant date tips match the observed
                            if m in extantsamples:
                                age = 0
                            newline = line.strip().replace(find.group(0),str(age))  
                        print >> outfile, newline
                    else:
                        print >> outfile, line.strip()