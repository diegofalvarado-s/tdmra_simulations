#script to plot sample dates

library(ape)

bison.fasta = read.FASTA('/home/diego/Dropbox (hickerlab)/ubuntu_runs/TDMRA_response/bison_data/bisonCar_60K.fasta')
bison.names = attributes(bison.fasta)$names
bison.dates = rep(NA, length(bison.names))
for (i in 1:length(bison.names)){
    bison.dates[i] = as.numeric(tail(strsplit(bison.names[i], '_')[[1]], 2)[1])
}
sample.dens = density(bison.dates, from=min(bison.dates), to=max(bison.dates))
sample.dens = data.frame(x=sample.dens$x, y=sample.dens$y)
sample.dens$y = unlist(lapply(sample.dens$y,function(x) (x*1)/sum(sample.dens$y)))
write.csv(sample.dens, 'Bison_dates_distribution.csv', row.names=F)

print(paste('Extant Samples Proportion Bison data:', length(which(as.numeric(bison.dates) == 0))/length(bison.dates)))
d = read.table('Sample_dates.tsv',header=F,sep='\t')
pdf('Date_comparison-BisonVsSimulated.pdf')
for (j in 1:nrow(d)){
    brks = hist(bison.dates, col=rgb(0,0,1,1/4), probability=T, main=paste0('Simulation',j))$breaks
    hist(as.numeric(d[j,]), breaks=brks, col=rgb(1,0,0,1/4), probability=T, add=T)
    legend('topright', c('Bison','Simulated'), col=(c(rgb(0,0,1,1/4), rgb(1,0,0,1/4))), pch=15)
    print(paste0('Extant Samples Proportion Run ', j, ': ', length(which(as.numeric(d[j,]) == 0))/length(d[j,])))
}
dev.off()