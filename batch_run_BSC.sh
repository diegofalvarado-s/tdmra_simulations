#!/bin/sh

for file in ./*-*.par
do
    echo Working on $file
    ./BayesSSC -f $file 1
done
